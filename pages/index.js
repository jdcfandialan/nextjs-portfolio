import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
        <Head>
            <title>John Dominic Fandialan</title>
            <link rel="icon" href="/favicon.ico" />
            <link href="https://fonts.googleapis.com/css2?family=Seaweed+Script&display=swap" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet" />
        </Head>

        <div className={styles.landingBody}>
            <h1 className={styles.title}>John Dominic Fandialan</h1>
            <h6 className={styles.subHeading}>Full-Stack Web Developer</h6>
        </div>

    </div>
  )
}
